## Tikee  
Shaun Janssens en Sander Van Damme  
  
*New Media Design & Development II*  
*Bachelor in de grafische en digitale media 2015-2016*  
*Multimediaproductie proDEV Arteveldehogeschool*  
  
---  
  
Links productiedossier  
1. [Academische Poster](docs/academische_poster.pdf)  
2. [Checklist](docs/checklist.md)  
3. [Productiedossier](docs/productiedossier.pdf)  
4. [Presentatie](docs/presentatie.pdf)  
5. [Timesheet Shaun](docs/timesheet_shaun_janssens.pdf)  
6. [Timesheet Sander](docs/timesheet_sander_vandamme.pdf)